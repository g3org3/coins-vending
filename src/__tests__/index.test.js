const { VendingMachine, inputTypes } = require('../vendingMachine')

describe('Vending Machine', () => {
  it('input: `wmcm` should return [ 2, 3, 1, 0 ]', async () => {
    const vending = new VendingMachine()
    const expected = [ 2, 3, 1, 0 ]
    const outputs = await Promise.all([
      vending.insert({ code: inputTypes.water }),
      vending.insert({ code: inputTypes.money }),
      vending.insert({ code: inputTypes.coffee }),
      vending.insert({ code: inputTypes.money })
    ])
    expect(outputs).toEqual(expected)
  })

  it('input: `wmcc` should return [ 2, 3, 1, 0 ]', async () => {
    const vending = new VendingMachine()
    const expected = [ 2, 3, 1, 0 ]
    const outputs = await Promise.all([
      vending.insert({ code: inputTypes.water }),
      vending.insert({ code: inputTypes.money }),
      vending.insert({ code: inputTypes.coffee }),
      vending.insert({ code: inputTypes.coffee })
    ])
    expect(outputs).toEqual(expected)
  })

  it('input: `wmcw` should return [ 2, 3, 1, 0 ]', async () => {
    const vending = new VendingMachine()
    const expected = [ 2, 3, 1, 0 ]
    const outputs = await Promise.all([
      vending.insert({ code: inputTypes.water }),
      vending.insert({ code: inputTypes.money }),
      vending.insert({ code: inputTypes.coffee }),
      vending.insert({ code: inputTypes.water })
    ])
    expect(outputs).toEqual(expected)
  })

  it('input: `wmc` should return [ 2, 3, 1 ]', async () => {
    const vending = new VendingMachine()
    const expected = [ 2, 3, 1 ]
    const outputs = await Promise.all([
      vending.insert({ code: inputTypes.water }),
      vending.insert({ code: inputTypes.money }),
      vending.insert({ code: inputTypes.coffee })
    ])
    expect(outputs).toEqual(expected)
  })

  it('input: `wcc` should return [ 2, 1, 0 ]', async () => {
    const vending = new VendingMachine()
    const expected = [ 2, 1, 0 ]
    const outputs = await Promise.all([
      vending.insert({ code: inputTypes.water }),
      vending.insert({ code: inputTypes.coffee }),
      vending.insert({ code: inputTypes.coffee })
    ])
    expect(outputs).toEqual(expected)
  })

  it('input: `wcw` should return [ 2, 1, 0 ]', async () => {
    const vending = new VendingMachine()
    const expected = [ 2, 1, 0 ]
    const outputs = await Promise.all([
      vending.insert({ code: inputTypes.water }),
      vending.insert({ code: inputTypes.coffee }),
      vending.insert({ code: inputTypes.water })
    ])
    expect(outputs).toEqual(expected)
  })

  it('input: `wmm` should return [ 2, 3, 0 ]', async () => {
    const vending = new VendingMachine()
    const expected = [ 2, 3, 0 ]
    const outputs = await Promise.all([
      vending.insert({ code: inputTypes.water }),
      vending.insert({ code: inputTypes.money }),
      vending.insert({ code: inputTypes.money })
    ])
    expect(outputs).toEqual(expected)
  })

  it('input: `wmc` should return [ 2, 3, 1 ]', async () => {
    const vending = new VendingMachine()
    const expected = [ 2, 3, 1 ]
    const outputs = await Promise.all([
      vending.insert({ code: inputTypes.water }),
      vending.insert({ code: inputTypes.money }),
      vending.insert({ code: inputTypes.coffee })
    ])
    expect(outputs).toEqual(expected)
  })

  it('input: `wmw` should return [ 2, 3, 0 ]', async () => {
    const vending = new VendingMachine()
    const expected = [ 2, 3, 0 ]
    const outputs = await Promise.all([
      vending.insert({ code: inputTypes.water }),
      vending.insert({ code: inputTypes.money }),
      vending.insert({ code: inputTypes.water })
    ])
    expect(outputs).toEqual(expected)
  })

  it('input: `cm` should return [ 1, 3 ]', async () => {
    const vending = new VendingMachine()
    const expected = [ 1, 3 ]
    const outputs = await Promise.all([
      vending.insert({ code: inputTypes.coffee }),
      vending.insert({ code: inputTypes.money })
    ])
    expect(outputs).toEqual(expected)
  })

  it('input: `cc` should return [ 1, 0 ]', async () => {
    const vending = new VendingMachine()
    const expected = [ 1, 0 ]
    const outputs = await Promise.all([
      vending.insert({ code: inputTypes.coffee }),
      vending.insert({ code: inputTypes.coffee })
    ])
    expect(outputs).toEqual(expected)
  })

  it('input: `cw` should return [ 1, 2 ]', async () => {
    const vending = new VendingMachine()
    const expected = [ 1, 2 ]
    const outputs = await Promise.all([
      vending.insert({ code: inputTypes.coffee }),
      vending.insert({ code: inputTypes.water })
    ])
    expect(outputs).toEqual(expected)
  })
})
