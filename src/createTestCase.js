
const Enquirer = require('enquirer')
const enquirer = new Enquirer()
enquirer.register('radio', require('prompt-radio'))
enquirer.register('text', require('prompt-text'))
const { log } = console

const question = [
  {
    name: 'code',
    type: 'text',
    message: 'enter test case:'
  },
  {
    name: 'outputs',
    type: 'text',
    message: 'expected output:'
  }
]

const inputTypes = {
  w: 'water',
  c: 'coffee',
  m: 'money'
}

function str2Array (str) {
  const list = []
  while (str.length !== 0) {
    list.push(str.substr(0, 1))
    str = str.substr(1)
  }
  return list
}

async function Main () {
  const { code, outputs } = await enquirer.ask(question)
  const testCode = generateTC(str2Array(code), str2Array(outputs))
  log(testCode)
}

function generateTC (code, outputs) {
  const inputs = code.map(i => {
    return `vending.insert({ code: inputTypes.${inputTypes[i]} })`
  })
  const template = `
it('input: \`${code.join('')}\` should return [ ${outputs.join(', ')} ]', async () => {
  const vending = new VendingMachine()
  const expected = [ ${outputs.join(', ')} ]
  const outputs = await Promise.all([
    ${inputs.join(',\n    ')}
  ])
  expect(outputs).toEqual(expected)
})`

  return template
}

Main()
