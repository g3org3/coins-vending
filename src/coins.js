const inquirer = require('inquirer')
const { log } = console

const state = {
  total: 0,
  id: 0
}

const prompt = message =>
  inquirer.prompt([
    {
      name: 'res',
      message,
      type: 'checkbox',
      choices: [{ name: 'nickle' }, { name: 'dime' }]
    }
  ])

const changeState = coin => {
  if (state.total >= 20) {
    return { output: 20 }
  } else if (coin === 'nickle') {
    state.total += 5
  } else {
    state.total += 10
  }
  return { output: state.total }
}

async function Main () {
  const { res } = await prompt('Enter coin')
  let [coin] = res
  while (coin) {
    const { output } = changeState(coin)
    log('---------------------')
    log('input :', coin)
    log('output:', output)
    log('---------------------')
    const { res } = await prompt('Enter coin')
    coin = res[0]
  }
  log('End')
}

exports.Main = Main
