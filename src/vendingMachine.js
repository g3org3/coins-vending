const Enquirer = require('enquirer')
const enquirer = new Enquirer()
enquirer.register('radio', require('prompt-radio'))
const { log } = console

const question = [
  {
    name: 'code',
    type: 'radio',
    choices: ['coffee', 'water', 'money'],
    message: 'select an input:'
  }
]

const outputTypes = {
  coffee: 1,
  water: 2,
  money: 3
}

exports.inputTypes = {
  coffee: 'coffee',
  water: 'water',
  money: 'money'
}

function ask (input) {
  return ((input) ? Promise.resolve(input) : enquirer.ask(question))
}

function VendingMachine (input) {
  this.state = {
    coffee: 0,
    water: 0,
    money: 0
  }
  this.output = 0

  this.insert = input => ask(input).then(({ code }) => {
    this.output = 0
    if (!this.state[code]) {
      this.state[code] += 1
      this.output = outputTypes[code]
    }
    // if (code === 'water') {
    //   this.state[code] += 1
    //   this.output = outputTypes[code]
    // }
    return Promise.resolve(this.output)
  })

  return this
}

async function Main () {
  const vending = new VendingMachine()
  while (true) {
    const output = await vending.insert()
    log('output:', output)
  }
}

exports.Main = Main
exports.VendingMachine = VendingMachine
