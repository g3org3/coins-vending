[![Build Status][travis]][travis-url]
[![npm package][npm-image]](npm-url)
# vending-machine
a brief description

## Getting Started
You will need Node >= 7 installed. [How do I install node? click here to find out about nvm](https://github.com/creationix/nvm#installation)

### Installation
```sh
npm install -g 
```

# Usage

## Development
<!--
Fork, then clone the repo:
```sh
git clone https://github.com/your-username/vending-machine.git
cd vending-machine
git remote set-url g3 https://github.com/g3org3/vending-machine.git
npm install
npm link
vending-machine --help
```
-->

## Changelog
<!--
[https://github.com/g3org3/vending-machine/blob/master/CHANGELOG.md](https://github.com/g3org3/vending-machine/blob/master/CHANGELOG.md)
-->

## Contributors
* George <7jagjag@gmail.com>

<!--
[travis]: https://travis-ci.org/g3org3/vending-machine.svg?branch=master
[travis-url]: https://travis-ci.org/g3org3/vending-machine
[npm-image]: https://img.shields.io/npm/v/vending-machine.svg?style=flat-square
[npm-url]: https://www.npmjs.org/package/vending-machine
-->
